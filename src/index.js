import React from 'react'
import ReactDom from 'react-dom'

// Nested Components, React Tools
import './index.css'
function BookList(){
  return(
    <section className='booklist'>
      <Book />
    </section>
  );
}

const author = 'Sudha Murty';
const Book = () =>{
  const title = 'Grandmas bag of Stories';
  return (
  <article className='book'>
    <img 
    src="https://images-eu.ssl-images-amazon.com/images/I/91xAvyJmUUL._AC_UL200_SR200,200_.jpg" alt="" />
  <h1>{title}</h1>
  <h4>{author.toUpperCase()}</h4>
  {/* <p>{let x = 6}</p> */}
  <p>{6+6}</p>
  </article>
  );
};

ReactDom.render(<BookList/>, document.getElementById('root'));
 